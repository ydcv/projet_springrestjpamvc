package model;

import java.time.LocalDate;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@SequenceGenerator(name = "module_gen", sequenceName = "module_seq", initialValue = 100, allocationSize = 1)
public class Module {

	@Id
	@GeneratedValue(generator = "module_gen")
	private Integer id;

	private String libelle;

	private LocalDate dateDebut;

	private LocalDate dateFin;

	private Personne formateur;

	private Filiere filiere;

}
