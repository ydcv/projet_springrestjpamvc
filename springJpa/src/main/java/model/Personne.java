package model;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@SequenceGenerator(name = "personne_gen", sequenceName = "personne_seq", initialValue = 100, allocationSize = 1)
public class Personne {

	@Id
	@GeneratedValue(generator = "personne_gen")
	private Integer id;

	private String nom;

	private String prenom;

	private TypePersonne type;

	private Filiere filiere;

}
